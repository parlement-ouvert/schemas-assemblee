# Schémas Assemblée

Les Schémas XML des données de l'Assemblée nationale

À l'origine, ces schémas, fournis par l'Assemblée, décrivent un sur-ensemble des données publiées en open data sur le [site de l'Assemblée nationale](http://data.assemblee-nationale.fr).

Ces schémas ont ensuite été mis à jour afin de pouvoir valider l'open data de l'Assemblée et ainsi tenter de détecter des changements dans les données.

_Note :_ Les Schémas XML ont été reformatés en utilisant la commande :
```bash
find . -name "*.xsd" -type f | xargs -- ./xmltidy.sh
```

## Utilisation pour valider les fichiers open data XML de l'Assemblée.

```bash
# Dossiers législatifs
wget http://data.assemblee-nationale.fr/static/openData/repository/15/loi/dossiers_legislatifs/Dossiers_Legislatifs_XV.xml.zip
unzip Dossiers_Legislatifs_XV.xml.zip
rm Dossiers_Legislatifs_XV.xml.zip
xmllint --format Dossiers_Legislatifs_XV.xml > Dossiers_Legislatifs_XV_format.xml
mv Dossiers_Legislatifs_XV_format.xml Dossiers_Legislatifs_XV.xml
xmllint --noout --schema Schemas/export_dossiers_legislatifs.xsd Dossiers_Legislatifs_XV.xml

# Acteurs, (députés - sénateurs - ministres), nominations, organes
wget http://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_senateurs_ministres_legislature/AMO20_dep_sen_min_tous_mandats_et_organes_XV.xml.zip
unzip AMO20_dep_sen_min_tous_mandats_et_organes_XV.xml.zip
rm AMO20_dep_sen_min_tous_mandats_et_organes_XV.xml.zip
xmllint --format AMO20_dep_sen_min_tous_mandats_et_organes_XV.xml > AMO20_dep_sen_min_tous_mandats_et_organes_XV_format.xml
mv AMO20_dep_sen_min_tous_mandats_et_organes_XV_format.xml AMO20_dep_sen_min_tous_mandats_et_organes_XV.xml
xmllint --noout --schema Schemas/export_organes_acteurs.xsd AMO20_dep_sen_min_tous_mandats_et_organes_XV.xml

# Amendements
wget http://data.assemblee-nationale.fr/static/openData/repository/15/loi/amendements_legis/Amendements_XV.xml.zip
unzip Amendements_XV.xml.zip
rm Amendements_XV.xml.zip
xmllint --format Amendements_XV.xml > Amendements_XV_format.xml
mv Amendements_XV_format.xml Amendements_XV.xml
xmllint --noout --schema Schemas/export_amendements.xsd Amendements_XV.xml

# Votes
wget http://data.assemblee-nationale.fr/static/openData/repository/15/loi/scrutins/Scrutins_XV.xml.zip
unzip Scrutins_XV.xml.zip
rm Scrutins_XV.xml.zip
xmllint --format Scrutins_XV.xml > Scrutins_XV_format.xml
mv Scrutins_XV_format.xml Scrutins_XV.xml
xmllint --noout --schema Schemas/export_scrutins.xsd Scrutins_XV.xml

# Questions (orales) au gouvernement
wget http://data.assemblee-nationale.fr/static/openData/repository/15/questions/questions_gouvernement/Questions_gouvernement_XV.xml.zip
unzip Questions_gouvernement_XV.xml.zip
rm Questions_gouvernement_XV.xml.zip
xmllint --format Questions_gouvernement_XV.xml > Questions_gouvernement_XV_format.xml
mv Questions_gouvernement_XV_format.xml Questions_gouvernement_XV.xml
xmllint --noout --schema Schemas/export_questions_gouvernement.xsd Questions_gouvernement_XV.xml

# Questions orales sans débat
wget http://data.assemblee-nationale.fr/static/openData/repository/15/questions/questions_orales_sans_debat/Questions_orales_sans_debat_XV.xml.zip
unzip Questions_orales_sans_debat_XV.xml.zip
rm Questions_orales_sans_debat_XV.xml.zip
xmllint --format Questions_orales_sans_debat_XV.xml > Questions_orales_sans_debat_XV_format.xml
mv Questions_orales_sans_debat_XV_format.xml Questions_orales_sans_debat_XV.xml
xmllint --noout --schema Schemas/export_questions_orales_sans_debat.xsd Questions_orales_sans_debat_XV.xml

# Questions écrites
wget http://data.assemblee-nationale.fr/static/openData/repository/15/questions/questions_ecrites/Questions_ecrites_XV.xml.zip
unzip Questions_ecrites_XV.xml.zip
rm Questions_ecrites_XV.xml.zip
xmllint --format Questions_ecrites_XV.xml > Questions_ecrites_XV_format.xml
mv Questions_ecrites_XV_format.xml Questions_ecrites_XV.xml
xmllint --noout --schema Schemas/export_questions_ecrites.xsd Questions_ecrites_XV.xml

# Réunions
wget http://data.assemblee-nationale.fr/static/openData/repository/15/vp/reunions/Agenda_XV.xml.zip
unzip Agenda_XV.xml.zip
rm Agenda_XV.xml.zip
xmllint --format Agenda_XV.xml > Agenda_XV_format.xml
mv Agenda_XV_format.xml Agenda_XV.xml
xmllint --noout --schema Schemas/export_agenda.xsd Agenda_XV.xml
```