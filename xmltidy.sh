#! /bin/sh
#  Cf https://unix.stackexchange.com/questions/50198/reformatting-a-large-number-of-xml-files
# Usage:
#   find . -name "*.xsd" -type f | xargs -- ./xmltidy.sh
for file
do
   xmllint --format $file > $file.tmp && mv $file.tmp $file
done
